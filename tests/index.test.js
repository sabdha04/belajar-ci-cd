const request = require('supertest');
const app = require('../index');

describe('API Endpoints', () => {
    
    test('list all todos', async () => {
        const response = await request(app).get('/api/todos');
        expect(response.status).toBe(200);
        expect(response.body).toBeInstanceOf(Array);
    });

    test('create a new todo', async () => {
        const response = await request(app)
            .post('/api/todos')
            .send({
                title: 'New Todo'
            });
        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body.title).toBe('New Todo');
    });

    test('specific todo', async () => {
        const response = await request(app).get('/api/todos/1');
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
    });

    test('delete a todo', async () => {
        const response = await request(app).delete('/api/todos/6');
        expect(response.status).toBe(200);
        expect(response.body.message).toBe('Todo deleted');
    });
});