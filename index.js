const express = require('express');
const app = express();
const port = 3000;

// Impor model dan routers
const env = require('./models/todo'); 
const todosRouter = require('./routers/todos'); 

app.use(express.json());

// Menggunakan router untuk rute terkait todos
app.use(todosRouter);


if (env !== "test") {
    app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });
}

module.exports = app;
