const { DataTypes, Sequelize } = require('sequelize'); // Impor Sequelize dari modul sequelize
const config = require('../config/config.json'); // Impor konfigurasi database dari config.json

const env = process.env.NODE_ENV || 'development'; 
const sequelize = new Sequelize(config[env].database, config[env].username, config[env].password, {
    host: config[env].host,
    dialect: config[env].dialect,
});

const Todo = sequelize.define('Todo', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

module.exports = Todo;
