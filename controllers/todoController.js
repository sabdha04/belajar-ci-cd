const Todo = require('../models/todo');

exports.listAllTodos = async (req, res) => {
    try {
        const todos = await Todo.findAll();
        res.json(todos);
    } catch (error) {
        console.error('Error fetching todos:', error);
        res.status(500).json({
            error: 'Internal Server Error'
        });
    }
};

exports.getTodoById = async (req, res) => {
    const {
        id
    } = req.params;
    try {
        const todo = await Todo.findByPk(id);
        if (todo) {
            res.json(todo);
        } else {
            res.status(404).json({
                error: 'Todo not found'
            });
        }
    } catch (error) {
        console.error('Error fetching todo:', error);
        res.status(500).json({
            error: 'Internal Server Error'
        });
    }
};

exports.createTodo = async (req, res) => {
    const {
        title
    } = req.body;
    try {
        const todo = await Todo.create({
            title
        });
        res.status(201).json(todo);
    } catch (error) {
        console.error('Error creating todo:', error);
        res.status(500).json({
            error: 'Internal Server Error'
        });
    }
};

exports.deleteTodo = async (req, res) => {
    const {
        id
    } = req.params;
    try {
        const todo = await Todo.findByPk(id);
        if (todo) {
            await todo.destroy();
            res.json({
                message: 'Todo deleted'
            });
        } else {
            res.status(404).json({
                error: 'Todo not found'
            });
        }
    } catch (error) {
        console.error('Error deleting todo:', error);
        res.status(500).json({
            error: 'Internal Server Error'
        });
    }
};