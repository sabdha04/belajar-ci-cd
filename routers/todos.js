const express = require('express');
const router = express.Router();
const todoController = require('../controllers/todoController');

// API untuk - List All Todo
router.get('/api/todos', todoController.listAllTodos);

// API untuk - Detail Todo
router.get('/api/todos/:id', todoController.getTodoById);

// API untuk - Create Todo
router.post('/api/todos', todoController.createTodo);

// API untuk - Delete Todo (Soft Delete)
router.delete('/api/todos/:id', todoController.deleteTodo);

module.exports = router;
